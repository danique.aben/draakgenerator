const axios = require('axios');

function GenerateDragons (callback, min, max, mutation, father, mother)
{
    let params = [];

    if (min)
    {
        if (max)
        {
            if (min != max)
            {
                params.push("amountmin=" + min);
                params.push("amountmax=" + max);
            }
            else
            {
                params.push("amount=" + min);
            }
        }
        else
        {
            params.push("amount=" + min);
        }
    }

    if (mutation || mutation === 0)
    {
        params.push("mutationchance=" + mutation);
    }

    if (father)
    {
        params.push("father=" + father);
    }

    if (mother)
    {
        params.push("mother=" + mother);
    }

    let str = "?" + params.join("&");
    let url = 'https://projects.timfalken.com/dragongenerator/api.php' + str;

    console.log("URL", url);
    axios.get(url).then(function (response)
    {
        callback(response.data);
    });
}

module.exports = GenerateDragons;

/*
GenerateDragons( function ( drgns )
{
    console.log( drgns );
}
    , 5 );*/