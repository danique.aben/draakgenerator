const url = "https://discordapp.com/oauth2/authorize?&client_id=1079793888564359248&scope=bot&permissions=2147747840";
const TOKEN = "MTA3OTc5Mzg4ODU2NDM1OTI0OA.GGVaai.zjXEijb34-ND1omU_lgq28WxAe3taJe6XBykZw";
const CLIENT_ID = "1079793888564359248";

const { REST, Routes } = require('discord.js');


const { Client, GatewayIntentBits } = require('discord.js');
const { randomBytes } = require('crypto');
const client = new Client({ intents: [GatewayIntentBits.Guilds] });
var toHex = require('colornames');

var data = {};

const commands = [
    {
        name: 'ping',
        description: 'Replies with Pong!',
        execute: function (interaction)
        {
            interaction.reply('Pong!');
        }
    },
    {
        name: 'update',
        description: 'Updates and reloads the bot!',
        execute: function (interaction)
        {
            interaction.reply('Updating!');

            please.crash.the.bot.now();
        }
    },
    {
        name: 'generate',
        description: 'Generate some dragons!',
        options: [
            { type: 3, name: "min", description: "Minimum amount of dragons to generate!", required: true },
            { type: 3, name: "max", description: "Maximum amount of dragons to generate!", required: false },
            { type: 3, name: "mutation", description: "Mutation chance! Leave blank to use default (0-100).", required: false },
            { type: 3, name: "father", description: "The genetic string of the father", required: false },
            { type: 3, name: "mother", description: "The genetic string of the mother", required: false }
        ],
        execute: function (interaction)
        {
            const GenerateDragons = require("./getdragon.js");

            let min = interaction.options.get("min")?.value;
            let max = interaction.options.get("max")?.value || min;
            let mutation = interaction.options.get("mutation")?.value;

            console.log("min", min);
            console.log("max", max);
            console.log("mutation", mutation);

            let father = interaction.options.get("father")?.value;
            let mother = interaction.options.get("mother")?.value;
            GenerateDragons(function (data)
            {
                let dragonEmbeds = [];

                for (let i = 0; i < data.dragons.length; i++)
                {
                    let colorname = (data.dragons[i].color);

                    if (colorname == "bronze")
                    {
                        colorname = "orange";
                    }

                    if (colorname == "gold")
                    {
                        colorname = "yellow";
                    }

                    let color = toHex(colorname.toLowerCase());
                    if (!color)
                        color = "#bbbbbb";

                    color = color.replace("#", "0x");

                    let newDragon = {
                        title: (data.dragons[i].gender == "Male" ? "♂️" : "♀️") + " " + data.dragons[i].shade + " " + data.dragons[i].color + " Dragon.",
                        description: "Build: " + data.dragons[i].build + "\n" + (data.dragons[i].gender == "Male" ? "" : "Cluch Sizes: " + data.dragons[i].clutchSizes + "\n")
                            + "Wing Shape: " + data.dragons[i].wingshape + "\nSize: " + data.dragons[i].size + "\nWingspan: " + data.dragons[i].wingSpan
                            + (data.dragons[i].secondColor ? "\nSecond Color: " + data.dragons[i].secondColor : "")
                            + (data.dragons[i].thirdColor ? "\nThird Color: " + data.dragons[i].thirdColor : "")
                            + (data.dragons[i].mutation ? "\nMutation: " + data.dragons[i].mutation : "")
                            + (data.dragons[i].geneticDefect ? "\nGenetic Defect: " + data.dragons[i].geneticDefect : "")
                            + "\n\nGenetic String: " + data.dragons[i].geneticString,
                        color: eval(color)
                    };

                    dragonEmbeds.push(newDragon);
                }

                if (dragonEmbeds.length <= 10)
                {
                    interaction.reply({ embeds: dragonEmbeds });
                }
                else
                {
                    let sliced = dragonEmbeds.splice(0, 10);
                    interaction.reply({ embeds: sliced }).then(function ()
                    {
                        while (dragonEmbeds.length > 10)
                        {
                            sliced = dragonEmbeds.splice(0, 10);
                            interaction.followUp({ embeds: sliced });
                        }

                        if (dragonEmbeds.length > 0)
                            interaction.followUp({ embeds: dragonEmbeds });
                    });


                }
            }, min, max, mutation, father, mother);
        }
    }
];

const rest = new REST({ version: '10' }).setToken(TOKEN);

(async () =>
{
    try
    {
        console.log('Started refreshing application (/) commands.');

        await rest.put(Routes.applicationCommands(CLIENT_ID), { body: commands });

        console.log('Successfully reloaded application (/) commands.');
    }
    catch (error)
    {
        console.error(error);
    }
})();

client.on('ready', () =>
{
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('interactionCreate', function (interaction)
{
    if (!interaction.isChatInputCommand()) return;

    for (let i = 0; i < commands.length; i++)
    {
        if (commands[i].name === interaction.commandName)
        {
            console.log("Current Data:", data);
            commands[i].execute(interaction);
            return;
        }
    }
});

client.login(TOKEN);